Instructions of how to build, install and use kilogram to pound project:

Create a Console .NET Framework project in Visual Studio.
Copy the code from the file and paste it in inside the .cs file
Run the code by clicking on start option in Visual Studio
Enter weight (kgs) in numbers and press enter.

License for this project: Proprietary license
Terms:
No one can copy this project
No one can share this project
No one is allowed to edit this project
This license is chosen because no external resources were used in creating the project